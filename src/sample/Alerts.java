package sample;

/**
 * Created by Alexey on 11.05.2017.
 */

import javafx.scene.control.Alert;

public class Alerts {

    public static void show(String text) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("");
        alert.setContentText(text);
        alert.setHeaderText("");
        alert.showAndWait();
    }
}
