package sample.Controller;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.Filters.FilterSet;

import java.awt.image.BufferedImage;

/**
 * Created by Alexey on 12.05.2017.
 */
public class BlurController {
    public static Stage stage;
    public static BufferedImage img;
    public static ImageView imageView;
    @FXML
    private
    Button okBtn, cancelBtn, applyBtn;
    @FXML
    private
    Slider pwrSlider, radSlider;
    @FXML
    private
    TextField radFld, pwrFld;
    @FXML
    private
    ProgressBar progress;


    @FXML
    private void initialize() {
        Task<Integer> task = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                int max = 100;
                while (progress.getProgress() < 100) {
                    Thread.sleep(30);
                    Platform.runLater(() -> updateProgress(FilterSet.progress, max));
                }
                return max;
            }
        };
        stage.setOnCloseRequest(event -> stage.close());

        applyBtn.setOnAction(new EventHandler<ActionEvent>() {
            BufferedImage tempImage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);

            @Override
            public void handle(ActionEvent event) {
                Platform.runLater(() -> {
                    Thread handler =
                            new Thread(() -> {
                                tempImage = FilterSet.blur(img, pwrSlider.getValue(), (int) radSlider.getValue());
                                imageView.setImage(SwingFXUtils.toFXImage(tempImage, null));

                            });
                    handler.setPriority(10);
                    handler.start();
                });
                progress.progressProperty().bind(task.progressProperty());
                Thread update = new Thread(task);
                update.setPriority(10);
                update.start();
            }
        });


        okBtn.setOnAction(event -> {
            img = FilterSet.blur(img, pwrSlider.getValue(), (int) radSlider.getValue());
            stage.close();
        });

        cancelBtn.setOnAction(event -> {
            imageView.setImage(SwingFXUtils.toFXImage(img, null));
            stage.close();
        });


        pwrSlider.maxProperty().set(30);
        pwrSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            pwrFld.setText(String.valueOf(newValue.intValue()));
            progress.progressProperty().unbind();
            progress.setProgress(0);
        });
        radSlider.setShowTickLabels(true);
        radSlider.setShowTickMarks(true);
        radSlider.maxProperty().set(50);
        radSlider.setMajorTickUnit(25);
        radSlider.setBlockIncrement(5);
        radSlider.setMinorTickCount(0);
        radSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            progress.progressProperty().unbind();
            progress.setProgress(0);
            radFld.setText(String.valueOf(newValue.intValue()));

        });
        radFld.setText("0");
        radFld.setOnAction(event -> {
            int value = Integer.parseInt(radFld.getText());
            radSlider.valueProperty().set(value);
            progress.progressProperty().unbind();
            progress.setProgress(0);
        });
        pwrFld.setText("0");
        pwrFld.setOnAction(event -> {
            double value = Double.parseDouble(pwrFld.getText());
            pwrSlider.valueProperty().set(value);
            progress.progressProperty().unbind();
            progress.setProgress(0);
        });

    }
}







