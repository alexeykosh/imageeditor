package sample.Controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.Filters.BrightContrast;

import java.awt.image.BufferedImage;

/**
 * Created by Alexey on 11.05.2017.
 */
public class BrightContrastController {
    public static Stage stage;
    public static BufferedImage img;
    public static ImageView imageView;

    @FXML
    Slider brightSlider, contrastSlider, BWSlider;
    @FXML
    TextField brightFld, contrastFld, bwFld;
    @FXML
    Button cancelBtn, okBtn;


    @FXML
    private void initialize() {
        BrightContrast brightContrast = new BrightContrast();

        cancelBtn.setOnAction(event -> {
            stage.close();
            imageView.setImage(SwingFXUtils.toFXImage(img, null));
        });
        okBtn.setOnAction(event -> {
            Controller.img = brightContrast.apply(img, (int) brightSlider.valueProperty().get(), (int) contrastSlider.valueProperty().get(), (int) BWSlider.valueProperty().get());
            stage.close();
        });

        brightSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            brightFld.setText(String.valueOf(newValue.intValue()));
            imageView.setImage(SwingFXUtils.toFXImage(
                    brightContrast.apply(img, (int) brightSlider.valueProperty().get(), (int) contrastSlider.valueProperty().get(), (int) BWSlider.valueProperty().get()),
                    null
            ));
        });

        contrastSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            contrastFld.setText(String.valueOf(newValue.intValue()));
            imageView.setImage(SwingFXUtils.toFXImage(
                    brightContrast.apply(img, (int) brightSlider.valueProperty().get(), (int) contrastSlider.valueProperty().get(), (int) BWSlider.valueProperty().get()),
                    null
            ));
        });
        BWSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            bwFld.setText(String.valueOf(newValue.intValue()));
            imageView.setImage(SwingFXUtils.toFXImage(
                    brightContrast.apply(img, (int) brightSlider.valueProperty().get(), (int) contrastSlider.valueProperty().get(), (int) BWSlider.valueProperty().get()),
                    null
            ));
        });

        bwFld.setOnAction(event -> {
            try {
                int value = Integer.parseInt(bwFld.getText());
                if (value > -256 && value < 256) {
                    BWSlider.valueProperty().setValue(value);
                }
            } catch (NumberFormatException e) {
                bwFld.setText("");
                e.printStackTrace();
            }
        });

        brightFld.setOnAction((ActionEvent event) -> {
            int value = Integer.parseInt(brightFld.getText());
            if (value > -256 && value < 256) {
                brightSlider.valueProperty().setValue(value);
            }
        });

        contrastFld.setOnAction(event -> {
            try {
                int value = Integer.parseInt(contrastFld.getText());
                if (value > -256 && value < 256) {
                    contrastSlider.valueProperty().setValue(value);
                }
            } catch (NumberFormatException e) {
                contrastFld.setText("");
            }
        });


        brightSlider.minProperty().set(-255);
        brightSlider.maxProperty().set(255);
        contrastSlider.minProperty().set(-255);
        contrastSlider.maxProperty().set(255);
        BWSlider.minProperty().set(-255);
        BWSlider.maxProperty().set(255);
        brightFld.setText("0");
        contrastFld.setText("0");
        bwFld.setText("0");

    }
}
