package sample.Controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.Filters.FilterSet;

import java.awt.image.BufferedImage;

/**
 * Created by Alexey on 12.05.2017.
 */
public class CondimentalController {
    public static Stage stage;
    public static BufferedImage img;
    public static ImageView imageView;
    @FXML
    Button okBtn, cancelBtn;
    @FXML
    TextField percentFld;
    @FXML
    Slider noiseSlider;

    @FXML
    private void initialize() {
        noiseSlider.maxProperty().set(0.4);
        stage.setOnShowing(event -> imageView.setImage(SwingFXUtils.toFXImage(FilterSet.greyCondimentNoise(img, 0),
                null)));
        stage.setOnCloseRequest(event -> stage.close());
        percentFld.setText("0");
        percentFld.setOnAction(event -> {
            double value = Double.parseDouble(percentFld.getText());
            noiseSlider.valueProperty().setValue(value);
        });
        cancelBtn.setOnAction(event -> stage.close());


        okBtn.setOnAction(event -> {
            Controller.img = FilterSet.greyCondimentNoise(img, noiseSlider.valueProperty().get());
            stage.close();
        });

        noiseSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            percentFld.setText(String.valueOf(newValue.doubleValue()));
            imageView.setImage(SwingFXUtils.toFXImage(FilterSet.greyCondimentNoise(img, newValue.doubleValue()),
                    null));
        });

    }

}
