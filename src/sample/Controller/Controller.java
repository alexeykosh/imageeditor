package sample.Controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sample.Alerts;
import sample.Filters.*;
import sample.Main;
import sample.Spectrum.FrequencyFilter;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Controller {
    public static BufferedImage img;
    @FXML
    private
    ImageView imageView;
    @FXML
    private
    MenuItem concluve, median, open, exit, saveAs, bright, hightColor, lowColor, cutColor, negative, wtf, black_white, blur, gaussNoise, salt_peper, extract, hide;
    @FXML
    private
    MenuItem spectrum;
    @FXML
    private
    MenuItem lowBatt, highBatt, lowGauss, highGauss;

    public static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public static BufferedImage aplly(BufferedImage image) {
        final int WIDTH = image.getWidth();
        final int HEIGHT = image.getHeight();
        double[][] kernel = {
                {0.000789, 0.006581, 0.013347, 0.006581, 0.000789},
                {0.006581, 0.054901, 0.111345, 0.054901, 0.006581},
                {0.013347, 0.111345, 0.225821, 0.111345, 0.013347},
                {0.006581, 0.054901, 0.111345, 0.054901, 0.006581},
                {0.000789, 0.006581, 0.013347, 0.006581, 0.000789}};
        int kernelWidth = 5;
        int kernelHeight = 5;
//        int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        int[][] redPixels = new int[WIDTH][HEIGHT];
        int[][] greenPixels = new int[WIDTH][HEIGHT];
        int[][] bluePixels = new int[WIDTH][HEIGHT];
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                Color color = new Color(image.getRGB(i, j));
                redPixels[i][j] = color.getRed();
                greenPixels[i][j] = color.getGreen();
                bluePixels[i][j] = color.getBlue();
            }
        }
        int[][] result = new int[WIDTH][HEIGHT];


        for (int x = 0; x < WIDTH; x++) {
            for (int y = 0; y < HEIGHT; y++) {
                int rSum = 0, gSum = 0, bSum = 0, kSum = 0;

                for (int i = 0; i < kernelWidth; i++) {
                    for (int j = 0; j < kernelHeight; j++) {
                        int pixelPosX = x + (i - (kernelWidth / 2));
                        int pixelPosY = y + (j - (kernelHeight / 2));
                        if ((pixelPosX < 0) ||
                                (pixelPosX >= image.getWidth()) ||
                                (pixelPosY < 0) ||
                                (pixelPosY >= image.getHeight())) continue;
                        int red = redPixels[pixelPosX][pixelPosY];
                        int green = redPixels[pixelPosX][pixelPosY];
                        int blue = redPixels[pixelPosX][pixelPosY];

                        double kernelVal = kernel[i][j];

                        rSum += red * kernelVal;
                        gSum += green * kernelVal;
                        bSum += blue * kernelVal;

                        kSum += kernelVal;
                    }
                }

                if (kSum <= 0) kSum = 1;

                //Контролируем переполнения переменных
                rSum /= kSum;
                if (rSum < 0) rSum = 0;
                if (rSum > 255) rSum = 255;

                gSum /= kSum;
                if (gSum < 0) gSum = 0;
                if (gSum > 255) gSum = 255;

                bSum /= kSum;
                if (bSum < 0) bSum = 0;
                if (bSum > 255) bSum = 255;

                //Записываем значения в результирующее изображение
                result[x][y] = new Color(rSum, gSum, bSum).getRGB();
            }
        }
        for (int i = 0; i < WIDTH; i++) {
            for (int j = 0; j < HEIGHT; j++) {
                image.setRGB(i, j, result[i][j]);
            }
        }
        return image;
    }

    @FXML
    private void initialize() throws IOException {
        open.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter filter1 = new FileChooser.ExtensionFilter("jpg", "*.jpg");
            FileChooser.ExtensionFilter filter2 = new FileChooser.ExtensionFilter("png", "*.png");
            FileChooser.ExtensionFilter filter3 = new FileChooser.ExtensionFilter("jpeg", "*.jpeg");
            fileChooser.getExtensionFilters().add(filter1);
            fileChooser.getExtensionFilters().add(filter2);
            fileChooser.getExtensionFilters().add(filter3);

            File file = fileChooser.showOpenDialog(Main.primaryStage);

            if (file != null) {
                readFile(file);
            }

        });
        saveAs.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter filter1 = new FileChooser.ExtensionFilter("jpg", "*.jpg");
            FileChooser.ExtensionFilter filter2 = new FileChooser.ExtensionFilter("png", "*.png");
            FileChooser.ExtensionFilter filter3 = new FileChooser.ExtensionFilter("jpeg", "*.jpeg");
            fileChooser.getExtensionFilters().add(filter1);
            fileChooser.getExtensionFilters().add(filter2);
            fileChooser.getExtensionFilters().add(filter3);

            File file = fileChooser.showSaveDialog(Main.primaryStage);

            if (file != null) {
                saveFile(img, fileChooser.getSelectedExtensionFilter().getDescription(), file);
            }
        });
        exit.setOnAction(event -> System.exit(0));
        bright.setOnAction(event -> {
            try {
                if (img != null) {
                    loadBrightContrast();
                } else {
                    Alerts.show("Загрузите изображение!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        black_white.setOnAction(event -> {
            BWF bwf = new BWF();
            imageView.setImage(SwingFXUtils.toFXImage(bwf.apply(img), null));

        });
        wtf.setOnAction(event -> {
            WTFF wtff = new WTFF();
            img = wtff.apply(img);
            imageView.setImage(SwingFXUtils.toFXImage(img, null));
        });
        negative.setOnAction(event -> {
            SimpleFilter filter = new Bit();
            img = filter.apply(img);
            imageView.setImage(SwingFXUtils.toFXImage(img, null));
        });
        blur.setOnAction(event -> {
            try {
                if (img != null) {
                    loadBlur();

                } else {
                    Alerts.show("Загрузите изображение!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        gaussNoise.setOnAction(event -> {
            try {
                loadGaussNoise();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        salt_peper.setOnAction(event -> {
            try {
                loadCondimentalNoise();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        hightColor.setOnAction(event -> {
            try {
                if (img != null) {
                    loadHightColor();
                } else {
                    Alerts.show("Загрузите изображение!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        hide.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter filter1 = new FileChooser.ExtensionFilter("jpg", "*.jpg");
            FileChooser.ExtensionFilter filter2 = new FileChooser.ExtensionFilter("png", "*.png");
            FileChooser.ExtensionFilter filter3 = new FileChooser.ExtensionFilter("jpeg", "*.jpeg");
            fileChooser.getExtensionFilters().add(filter1);
            fileChooser.getExtensionFilters().add(filter2);
            fileChooser.getExtensionFilters().add(filter3);

            File file = fileChooser.showOpenDialog(Main.primaryStage);

            if (file != null) {
                try {
                    hide(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        extract.setOnAction(event -> extract());
        lowColor.setOnAction(event -> {
            try {
                loadLowController();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        cutColor.setOnAction(event -> {
            try {
                loadCutColor();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        concluve.setOnAction(event -> {
            if (img != null) {
                imageView.setImage(SwingFXUtils.toFXImage(FilterSet.concluve(img), null));
            } else {
                Alerts.show("Загрузите изображение!");
            }
        });
        median.setOnAction(event -> {
            if (img != null) {
                loadMedian();
            } else {
                Alerts.show("Загрузите изображение!");
            }
        });
        spectrum.setOnAction(event -> imageView.setImage(SwingFXUtils.toFXImage(FrequencyFilter.spectrum(img), null)));
        lowBatt.setOnAction(event -> {
            if (img != null) {
                imageView.setImage(SwingFXUtils.toFXImage(FrequencyFilter.apply(img, "lowBatt"), null));
            } else {
                Alerts.show("Загрузите изображение!");
            }
        });
        lowGauss.setOnAction(event -> {
            if (img != null) {
                imageView.setImage(SwingFXUtils.toFXImage(FrequencyFilter.apply(img, "lowGauss"), null));
            } else {
                Alerts.show("Загрузите изображение!");
            }
        });
        highBatt.setOnAction(event -> {
            if (img != null) {
                imageView.setImage(SwingFXUtils.toFXImage(FrequencyFilter.apply(img, "highBatt"), null));
            } else {
                Alerts.show("Загрузите изображение!");
            }
        });
        highGauss.setOnAction(event -> {
            if (img != null) {
                imageView.setImage(SwingFXUtils.toFXImage(FrequencyFilter.apply(img, "highGauss"), null));
            } else {
                Alerts.show("Загрузите изображение!");
            }
        });


//
//        double[][] pixels = new double[1024][1024];
//        for (int i = 0; i < width; i++) {
//            for (int j = 0; j < height; j++) {
//                pixels[i][j] = (double) bufferedImage.getRGB(i, j);
//            }
//        }
//        int[] onePixels = ((DataBufferInt) bufferedImage.getRaster().getDataBuffer()).getData();
////        for (int i = 0; i < width; i++) {
////            for (int j = 0; j < height; j++) {
////                onePixels[j * width + i] *= Math.pow(-1, j * width + i);
////            }
////        }
//        FFT fft = new FFT(onePixels, width, height);
//        onePixels = fft.getPixels();
//        for (int i = 0; i < onePixels.length; i++) {
//            bufferedImage.getRaster().getDataBuffer().setElem(i, onePixels[i]);
//        }
//

//        FrequencyFilter.apply(bufferedImage,"lowBatt");


        //imageView.setImage(SwingFXUtils.toFXImage(bufferedImage, null));


    }

    private void loadMedian() {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        MedianController.img = this.img;
        MedianController.imageView = this.imageView;
        MedianController.stage = primaryStage;
        AnchorPane root = null;
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource("/fxml/Median.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        primaryStage.setTitle("Медианный фильтр");
        primaryStage.setScene(new Scene(root, 307, 154));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
    }

    private void loadBlur() throws IOException {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        BlurController.img = this.img;
        BlurController.imageView = this.imageView;
        BlurController.stage = primaryStage;
        AnchorPane root = FXMLLoader.load(getClass().getClassLoader().getResource("/fxml/Blur.fxml"));
        primaryStage.setTitle("Размытие");
        primaryStage.setScene(new Scene(root, 307, 176));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
    }

    private void loadCutColor() throws IOException {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        CutController.img = this.img;
        CutController.imageView = this.imageView;
        CutController.stage = primaryStage;
        AnchorPane root = FXMLLoader.load(getClass().getClassLoader().getResource("/fxml/CutColor.fxml"));
        primaryStage.setTitle("Cut Color");
        primaryStage.setScene(new Scene(root, 311, 176));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
    }

    private void loadHightColor() throws IOException {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        HightColorController.img = this.img;
        HightColorController.imageView = this.imageView;
        HightColorController.stage = primaryStage;
        AnchorPane root = FXMLLoader.load(getClass().getResource("/fxml/HightColor.fxml"));
        primaryStage.setTitle("Светлые тона");
        primaryStage.setScene(new Scene(root, 311, 176));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();


    }

    private void loadGaussNoise() throws IOException {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        GaussNoiseController.img = this.img;
        GaussNoiseController.imageView = this.imageView;
        GaussNoiseController.stage = primaryStage;
        AnchorPane root = FXMLLoader.load(getClass().getResource("/fxml/GaussNoise.fxml"));
        primaryStage.setTitle("Шум по Гауссу");
        primaryStage.setScene(new Scene(root, 311, 176));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
        for (int i = 0; i < root.getChildren().size(); i++) {
            System.out.println("INDEX = " + i + " " + root.getChildren().get(i));
        }

    }

    private void loadBrightContrast() throws IOException {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        BrightContrastController.img = this.img;
        BrightContrastController.imageView = this.imageView;
        BrightContrastController.stage = primaryStage;
        AnchorPane root = FXMLLoader.load(getClass().getResource("/fxml/brightContrast.fxml"));
        primaryStage.setTitle("");
        primaryStage.setScene(new Scene(root, 311, 176));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();


    }

    private void loadCondimentalNoise() throws IOException {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        CondimentalController.img = this.img;
        CondimentalController.imageView = this.imageView;
        CondimentalController.stage = primaryStage;
        AnchorPane root = FXMLLoader.load(getClass().getResource("/fxml/CondimentalNoise.fxml"));
        primaryStage.setTitle("Соль и перец");
        primaryStage.setScene(new Scene(root, 311, 176));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
        for (int i = 0; i < root.getChildren().size(); i++) {
            System.out.println("INDEX = " + i + " " + root.getChildren().get(i));
        }

    }

    private void loadLowController() throws IOException {
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
        Stage primaryStage = new Stage();
        LowController.img = this.img;
        LowController.imageView = this.imageView;
        LowController.stage = primaryStage;
        AnchorPane root = FXMLLoader.load(getClass().getResource("/fxml/LowColor.fxml"));
        primaryStage.setTitle("Темные тона");
        primaryStage.setScene(new Scene(root, 311, 176));
        primaryStage.setOnCloseRequest(event -> System.exit(0));
        primaryStage.show();
    }

    private void hide(File file) throws FileNotFoundException {
        Image image = new Image(new FileInputStream(file));
        int widht = (int) image.getWidth();
        int height = (int) image.getHeight();
        BufferedImage temp = new BufferedImage(widht, height, BufferedImage.TYPE_INT_ARGB);
        SwingFXUtils.fromFXImage(image, temp);

        img = Steganography.hide(img, temp);
        imageView.setImage(SwingFXUtils.toFXImage(img, null));

    }

    private void extract() {
        SimpleFilter stegan = new Steganography();
        img = stegan.apply(img);
        imageView.setImage(SwingFXUtils.toFXImage(img, null));
    }

    private void readFile(File file) {
        try {
            imageView.setImage(new Image(new FileInputStream(file)));
            int width = (int) imageView.getImage().getWidth();
            int height = (int) imageView.getImage().getHeight();
            img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            SwingFXUtils.fromFXImage(imageView.getImage(), img);
            imageView.setFitWidth(img.getWidth());
            imageView.setFitHeight(img.getHeight());
            Main.primaryStage.setWidth(imageView.getFitWidth());
            Main.primaryStage.setHeight(imageView.getFitHeight() + 53);

        } catch (FileNotFoundException e) {
            Alerts.show("Ошибка чтения файла!");
            e.printStackTrace();
        }
    }

    private void saveFile(BufferedImage img, String fileType, File file) {
        try {
            imageView.setImage(SwingFXUtils.toFXImage(img, null));
            ImageIO.write(img, "jpg", file);
        } catch (IOException e) {
            Alerts.show("Ошибка при сохранении файла!");
            e.printStackTrace();
        }
    }

}

