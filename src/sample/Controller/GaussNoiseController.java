package sample.Controller;


import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.Filters.FilterSet;

import java.awt.image.BufferedImage;

/**
 * Created by Alexey on 12.05.2017.
 */
public class GaussNoiseController {
    public static Stage stage;
    public static BufferedImage img;
    public static ImageView imageView;
    @FXML
    Button okBtn, cancelBtn;
    @FXML
    TextField percentFld;
    @FXML
    CheckBox greyCheckBox;
    @FXML
    Slider noiseSlader;

    @FXML
    private void initialize() {
        stage.setOnCloseRequest(event -> stage.close());
        percentFld.setText("0");
        percentFld.setOnAction(event -> {
            int value = Integer.parseInt(percentFld.getText());
            noiseSlader.valueProperty().setValue(value);
        });
        cancelBtn.setOnAction(event -> {
            imageView.setImage(SwingFXUtils.toFXImage(FilterSet.colorGaussianNoise(img, 0), null));
            stage.close();
        });
        greyCheckBox.setOnAction(event -> {
            if (greyCheckBox.isSelected()) {
                imageView.setImage(SwingFXUtils.toFXImage(FilterSet.greyGaussianNoise(img, 0), null));
            }
            if (!greyCheckBox.isSelected()) {
                imageView.setImage(SwingFXUtils.toFXImage(FilterSet.colorGaussianNoise(img, 0), null));
            }
        });
        okBtn.setOnAction(event -> {
            if (greyCheckBox.isSelected()) {
                Controller.img = FilterSet.greyGaussianNoise(img, noiseSlader.valueProperty().get());
            } else {
                Controller.img = FilterSet.colorGaussianNoise(img, noiseSlader.valueProperty().get());
            }
            stage.close();
        });

        noiseSlader.valueProperty().addListener((observable, oldValue, newValue) -> {
            percentFld.setText(String.valueOf(newValue.doubleValue()));
            if (greyCheckBox.isSelected() == true) {
                imageView.setImage(SwingFXUtils.toFXImage(FilterSet.greyGaussianNoise(img, newValue.doubleValue()),
                        null));
            } else {
                imageView.setImage(SwingFXUtils.toFXImage(FilterSet.colorGaussianNoise(img, newValue.doubleValue()),
                        null));
            }
        });

    }
}
