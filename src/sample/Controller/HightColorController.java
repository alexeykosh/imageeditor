package sample.Controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.Filters.Filter;
import sample.Filters.HightColour;

import java.awt.image.BufferedImage;

/**
 * Created by Alexey on 12.05.2017.
 */
public class HightColorController {
    public static Stage stage;
    public static BufferedImage img;
    public static ImageView imageView;

    @FXML
    Slider redSlider, greenSlider, blueSlider;
    @FXML
    TextField brightFld, contrastFld, bwFld;
    @FXML
    Button cancelBtn, okBtn;


    @FXML
    private void initialize() {
        Filter brightContrast = new HightColour();

        cancelBtn.setOnAction(event -> {
            stage.close();
            imageView.setImage(SwingFXUtils.toFXImage(img, null));
        });
        okBtn.setOnAction(event -> {
            Controller.img = brightContrast.apply(img, (int) redSlider.valueProperty().get(), (int) greenSlider.valueProperty().get(), (int) blueSlider.valueProperty().get());
            stage.close();
        });

        redSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            brightFld.setText(String.valueOf(newValue.intValue()));
            imageView.setImage(SwingFXUtils.toFXImage(
                    brightContrast.apply(img, (int) redSlider.valueProperty().get(), (int) greenSlider.valueProperty().get(), (int) blueSlider.valueProperty().get()),
                    null
            ));
        });

        greenSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            contrastFld.setText(String.valueOf(newValue.intValue()));
            imageView.setImage(SwingFXUtils.toFXImage(
                    brightContrast.apply(img, (int) redSlider.valueProperty().get(), (int) greenSlider.valueProperty().get(), (int) blueSlider.valueProperty().get()),
                    null
            ));
        });
        blueSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            bwFld.setText(String.valueOf(newValue.intValue()));
            imageView.setImage(SwingFXUtils.toFXImage(
                    brightContrast.apply(img, (int) redSlider.valueProperty().get(), (int) greenSlider.valueProperty().get(), (int) blueSlider.valueProperty().get()),
                    null
            ));
        });

        bwFld.setOnAction(event -> {
            try {
                int value = Integer.parseInt(bwFld.getText());
                if (value > -256 && value < 256) {
                    blueSlider.valueProperty().setValue(value);
                }
            } catch (NumberFormatException e) {
                bwFld.setText("");
                e.printStackTrace();
            }
        });

        brightFld.setOnAction((ActionEvent event) -> {
            int value = Integer.parseInt(brightFld.getText());
            if (value > -256 && value < 256) {
                redSlider.valueProperty().setValue(value);
            }
        });

        contrastFld.setOnAction(event -> {
            try {
                int value = Integer.parseInt(contrastFld.getText());
                if (value > -256 && value < 256) {
                    greenSlider.valueProperty().setValue(value);
                }
            } catch (NumberFormatException e) {
                contrastFld.setText("");
            }
        });


        redSlider.maxProperty().set(255);
        greenSlider.maxProperty().set(255);
        blueSlider.maxProperty().set(255);
        brightFld.setText("0");
        contrastFld.setText("0");
        bwFld.setText("0");
    }
}
