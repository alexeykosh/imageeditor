package sample.Controller;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.Filters.FilterSet;

import java.awt.image.BufferedImage;

/**
 * Created by Alexey on 12.05.2017.
 */
public class MedianController {

    public static Stage stage;
    public static BufferedImage img;
    public static ImageView imageView;
    @FXML
    private
    Button okBtn, cancelBtn, applyBtn;
    @FXML
    private
    Slider radSlider;
    @FXML
    private
    TextField radFld;
    @FXML
    private
    ProgressBar progress;

    @FXML
    private void initialize() {
        final BufferedImage[] tempImage = {new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB)};
        Task<Integer> task = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                int max = 100;
                while ((int) progress.getProgress() < 100) {
                    Thread.sleep(30);
                    Platform.runLater(() -> updateProgress(FilterSet.progress, max));
                }
                return max;
            }
        };
        radFld.setText("0");
        applyBtn.setOnAction(event -> {
            progress.progressProperty().bind(task.progressProperty());
            Platform.runLater(() -> {
                Thread handler =
                        new Thread(() -> {
                            tempImage[0] = FilterSet.median(img, (int) radSlider.getValue());
                            imageView.setImage(SwingFXUtils.toFXImage(tempImage[0], null));
                        });
                handler.setPriority(10);
                handler.start();
            });
            Thread update = new Thread(task);
            update.setPriority(10);
            update.start();
        });
        okBtn.setOnAction(event -> {
            img = tempImage[0];
            stage.close();
        });

        cancelBtn.setOnAction(event -> {
            setImage(imageView, img);
            stage.close();
        });
        radSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            radFld.setText(String.valueOf(newValue.intValue()));
            progress.progressProperty().unbind();
            progress.setProgress(0);
        });
        radFld.setOnAction(event -> {
            int value = Integer.parseInt(radFld.getText());
            radSlider.valueProperty().set(value);
            progress.progressProperty().unbind();
            progress.setProgress(0);
        });
    }

    private void setImage(ImageView view, BufferedImage img) {
        view.setImage(SwingFXUtils.toFXImage(img, null));
    }
}
