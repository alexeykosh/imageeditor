package sample.Filters;

import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;


public class Bit implements SimpleFilter {
    @Override
    public BufferedImage apply(BufferedImage bufferedImage) {
        BufferedImage tempImage = Controller.deepCopy(bufferedImage);
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color(tempImage.getRGB(i, j));
                color = new Color(color.getRed() & 128, color.getGreen() & 128, color.getBlue() & 128, color.getAlpha());
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
        return tempImage;
    }
}
