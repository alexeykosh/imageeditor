package sample.Filters;

import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;


public class BrightContrast implements Filter {
    //-255...255
    @Override
    public BufferedImage apply(BufferedImage bufferedImage, int bright, int contrast, int gray) {
        BufferedImage tempImage = Controller.deepCopy(bufferedImage);
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color((int) tempImage.getRGB(i, j));
                if (bright >= 0) {
                    color = new Color(Math.min(bright + color.getRed(), 255),
                            Math.min(bright + color.getGreen(), 255),
                            Math.min(bright + color.getBlue(), 255),
                            color.getAlpha());
                } else {
                    color = new Color(Math.max(bright + color.getRed(), 0),
                            Math.max(bright + color.getGreen(), 0),
                            Math.max(bright + color.getBlue(), 0),
                            color.getAlpha());
                }
                if (contrast >= 0) {
                    color = new Color(
                            color.getRed() + (delta((short) color.getRed())) * contrast / 255,
                            color.getGreen() + (delta((short) color.getGreen())) * contrast / 255,
                            color.getBlue() + (delta((short) color.getBlue())) * contrast / 255,
                            color.getAlpha());
                } else {
                    color = new Color(
                            color.getRed() + (128 - color.getRed()) * -1 * contrast / 255,
                            color.getGreen() + (128 - color.getGreen()) * -1 * contrast / 255,
                            color.getBlue() + (128 - color.getBlue()) * -1 * contrast / 255,
                            color.getAlpha()
                    );

                }
                if (gray >= 0) {
                    color = new Color(
                            color.getRed() + (255 - color.getRed()) * gray / 255,
                            color.getGreen() + (255 - color.getGreen()) * gray / 255,
                            color.getBlue() + (255 - color.getBlue()) * gray / 255,
                            color.getAlpha());
                } else {
                    color = new Color(
                            color.getRed() + color.getRed() * gray / 255,
                            color.getGreen() + color.getGreen() * gray / 255,
                            color.getBlue() + color.getBlue() * gray / 255,
                            color.getAlpha());

                }
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
        return tempImage;
    }

    private int delta(short x) {
        if (x > 128) {
            return 255 - x;
        } else if (x < 128) {
            return 0 - x;
        } else return 0;

    }
}