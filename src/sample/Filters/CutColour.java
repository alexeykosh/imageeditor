package sample.Filters;

import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;


public class CutColour implements Filter {
    //0...255
    @Override
    public BufferedImage apply(BufferedImage bufferedImage, int bright, int contrast, int gray) {
        BufferedImage tempImage = Controller.deepCopy(bufferedImage);
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color((int) tempImage.getRGB(i, j));
                color = new Color(
                        color.getRed() * (255 - bright) / 255,
                        color.getGreen() * (255 - contrast) / 255,
                        color.getBlue() * (255 - gray) / 255,
                        color.getAlpha());
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
        return tempImage;
    }
}
