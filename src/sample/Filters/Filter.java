package sample.Filters;


import java.awt.image.BufferedImage;


public interface Filter {
    BufferedImage apply(BufferedImage bufferedImage, int bright, int contrast, int gray);
}
