package sample.Filters;

import org.jtransforms.fft.DoubleFFT_2D;
import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;
import java.util.Random;


public class FilterSet {
    public static double progress = 0;

    public static BufferedImage blur(BufferedImage img, double sigma, int kernelsize) {
        progress = 0;
        BufferedImage tempImage = Controller.deepCopy(img);
        double[] kernel = createKernel(sigma, kernelsize);
        double progressValue = (double) 1 / (tempImage.getWidth() * img.getHeight()) * 100;
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                progress += progressValue;
                double overflow = 0;
                int counter = 0;
                int kernelhalf = (kernelsize - 1) / 2;
                double red = 0;
                double green = 0;
                double blue = 0;
                for (int k = i - kernelhalf; k < i + kernelhalf; k++) {
                    for (int l = j - kernelhalf; l < j + kernelhalf; l++) {
                        if (k < 0 || k >= tempImage.getWidth() || l < 0 || l >= img.getHeight()) {
                            counter++;
                            overflow += kernel[counter];
                            continue;
                        }

                        Color c = new Color(img.getRGB(k, l));
                        red += c.getRed() * kernel[counter];
                        green += c.getGreen() * kernel[counter];
                        blue += c.getBlue() * kernel[counter];
                        counter++;
                    }
                    counter++;
                }

                if (overflow > 0) {
                    red = 0;
                    green = 0;
                    blue = 0;
                    counter = 0;
                    for (int k = i - kernelhalf; k < i + kernelhalf; k++) {
                        for (int l = j - kernelhalf; l < j + kernelhalf; l++) {

                            if (k < 0 || k >= tempImage.getWidth() || l < 0 || l >= tempImage.getHeight()) {
                                counter++;
                                continue;
                            }

                            Color c = new Color(img.getRGB(k, l));
                            red += c.getRed() * kernel[counter] * (1 / (1 - overflow));
                            green += c.getGreen() * kernel[counter] * (1 / (1 - overflow));
                            blue += c.getBlue() * kernel[counter] * (1 / (1 - overflow));
                            counter++;
                        }
                        counter++;
                    }
                }
                tempImage.setRGB(i, j, new Color((int) red % 256, (int) green % 256, (int) blue % 256).getRGB());
            }
        }
        return tempImage;
    }

    @Deprecated
    public static BufferedImage definition(BufferedImage img) {
        BufferedImage tempImage = Controller.deepCopy(img);
        double[][] kernel = {
                {-1, -1, -1},
                {-1, 9, -1},
                {-1, -1, -1}
        };
        int width = tempImage.getWidth();
        int height = tempImage.getHeight();
        int[][] redPixels = new int[width][height];
        int[][] bluePixels = new int[width][height];
        int[][] greenPixels = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Color color = new Color(tempImage.getRGB(i, j));
                redPixels[i][j] = color.getRed();
                bluePixels[i][j] = color.getBlue();
                greenPixels[i][j] = color.getGreen();
            }
        }

        for (int i = 0; i < width - 3; i++) {
            for (int j = 0; j < height - 3; j++) {
                int rSum = 0, gSum = 0, bSum = 0;
                for (int x = 0; x < 3; x++) {
                    for (int y = 0; y < 3; y++) {
                        rSum += redPixels[i + x][j + y] * kernel[x][y];
                        gSum += greenPixels[i + x][j + y] * kernel[x][y];
                        bSum += bluePixels[i + x][j + y] * kernel[x][y];
                    }
                }
                tempImage.setRGB(i, j, new Color(Math.abs(rSum % 256), Math.abs(gSum % 256), Math.abs(bSum % 256)).getRGB());
            }
        }
        return tempImage;
    }

    @Deprecated
    public static void Butthurt(BufferedImage img) {
        int width = img.getWidth();
        int height = img.getHeight();

        double[][] kernel = new double[1024][1024];
        double D;
        double zeroD = 5;
        double H;
        double[][] green = new double[1024][1024];
        double[][] red = new double[1024][1024];
        double[][] blue = new double[1024][1024];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Color color = new Color(img.getRGB(i, j));

                green[i][j] = color.getGreen();
                green[i][j] *= Math.pow(-1, i + j);

                red[i][j] = color.getRed();
                red[i][j] *= Math.pow(-1, i + j);

                blue[i][j] = color.getBlue();
                blue[i][j] *= Math.pow(-1, i + j);
            }
        }
        DoubleFFT_2D fft = new DoubleFFT_2D(1024, 1024);

        fft.realForward(green);
        fft.realForward(blue);
        fft.realForward(red);
        double[][] dMatrix = new double[1024][1024];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                dMatrix[i][j] = Math.sqrt(Math.pow(i - (double) width / 2, 2) + Math.pow(j - (double) height / 2, 2));
            }
        }
        fft.realForward(dMatrix);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                H = 1 / (1 + (Math.pow((dMatrix[i][j] / zeroD), 4)));
                kernel[i][j] = H;
            }
        }

        fft.realForward(kernel);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                green[i][j] *= kernel[i][j];
                red[i][j] *= kernel[i][j];
                blue[i][j] *= kernel[i][j];
            }
        }
        fft.realInverse(green, true);
        fft.realInverse(red, true);
        fft.realInverse(blue, true);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                green[i][j] *= Math.pow(-1, i + j);

                red[i][j] *= Math.pow(-1, i + j);

                blue[i][j] *= Math.pow(-1, i + j);
                img.setRGB(i, j, new Color(
                        Math.abs((int) red[i][j]) % 256,
                        Math.abs((int) green[i][j]) % 256,
                        Math.abs((int) blue[i][j]) % 256).getRGB());
            }
        }


    }

    public static BufferedImage concluve(BufferedImage img) {
        BufferedImage tempImage = Controller.deepCopy(img);
        int width = tempImage.getWidth();
        int height = tempImage.getHeight();
        double[][] kernel = {
                {-1, -1, -1},
                {-1, 9, -1},
                {-1, -1, -1}
        };

        double factor = 1.0;
        double bias = 0.0;

        int filterWidth = kernel.length;
        int filterHeight = kernel.length;
        int[] pixels = ((DataBufferInt) tempImage.getRaster().getDataBuffer()).getData();
        int[] greenPixels = new int[pixels.length];
        int[] redPixels = new int[pixels.length];
        int[] bluePixels = new int[pixels.length];
        int[] result = new int[pixels.length];

        for (int i = 0; i < pixels.length; i++) {
            Color color = new Color(pixels[i]);
            greenPixels[i] = color.getGreen();
            redPixels[i] = color.getRed();
            bluePixels[i] = color.getBlue();
        }

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                double red = 0, green = 0, blue = 0;
                for (int filterY = 0; filterY < filterHeight; filterY++) {
                    for (int filterX = 0; filterX < filterWidth; filterX++) {

                        int imageX = (x - filterWidth / 2 + filterX + width) % width;
                        int imageY = (y - filterHeight / 2 + filterY + height) % height;
                        red += redPixels[imageY * width + imageX] * kernel[filterY][filterX];
                        blue += bluePixels[imageY * width + imageX] * kernel[filterY][filterX];
                        green += greenPixels[imageY * width + imageX] * kernel[filterY][filterX];

                        int finalRed = (int) Math.min(Math.max(factor * red + bias, 0), 255);
                        int finalGreen = (int) Math.min(Math.max(factor * green + bias, 0), 255);
                        int finalBlue = (int) Math.min(Math.max(factor * blue + bias, 0), 255);

                        result[y * width + x] = new Color(finalRed, finalGreen, finalBlue).getRGB();
                        tempImage.getRaster().getDataBuffer().setElem(y * width + x, new Color(finalRed, finalGreen, finalBlue).getRGB());

                    }
                }
            }
        }
        return tempImage;
    }

    public static BufferedImage median(BufferedImage img, int size) {
        progress = 0;
        BufferedImage tempImage = Controller.deepCopy(img);
        int width = tempImage.getWidth();
        int height = tempImage.getHeight();
        double progressValue = ((double) 1 / (width * height)) * 100;
        int[][] redPixels = new int[width + size][height + size];
        int[][] bluePixels = new int[width + size][height + size];
        int[][] greenPixels = new int[width + size][height + size];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                Color color = new Color(tempImage.getRGB(i, j));
                redPixels[i][j] = color.getRed();
                bluePixels[i][j] = color.getBlue();
                greenPixels[i][j] = color.getGreen();
            }
        }
        for (int i = width; i < width + size; i++) {
            for (int j = height; j < height + size; j++) {
                redPixels[i][j] = new Random().nextInt(256);
                bluePixels[i][j] = new Random().nextInt(256);
                greenPixels[i][j] = new Random().nextInt(256);
            }
        }
        int[] redSort = new int[size * size];
        int[] greenSort = new int[size * size];
        int[] blueSort = new int[size * size];
        if (size != 0) {
            int bound = 1;
            switch (size) {
                case 3:
                    bound = 1;
                    break;
                case 5:
                    bound = 1;
                    break;
                case 7:
                    bound = 2;
                    break;
                case 9:
                    bound = 3;
                    break;
                case 11:
                    bound = 4;
                    break;
            }
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    int index = 0;
                    for (int k = 0; k < size; k++) {
                        for (int l = 0; l < size; l++) {
                            redSort[index] = redPixels[i + k][j + l];
                            greenSort[index] = greenPixels[i + k][j + l];
                            blueSort[index] = bluePixels[i + k][j + l];
                            index++;
                        }
                    }
                    Arrays.sort(redSort);
                    Arrays.sort(greenSort);
                    Arrays.sort(blueSort);

                    tempImage.setRGB(i, j, new Color(redSort[size / 2 + 1], greenSort[size / 2 + 1], blueSort[size / 2 + 1]).getRGB());
                    progress += progressValue;
                }
            }

        }

        return tempImage;
    }

    public static BufferedImage colorGaussianNoise(BufferedImage img, double variance) {
        BufferedImage tempImage = Controller.deepCopy(img);
        int width = tempImage.getWidth();
        int height = tempImage.getHeight();
        Random random = new Random();
        int[] input = ((DataBufferInt) tempImage.getRaster().getDataBuffer()).getData();
        int[] green = new int[input.length];
        int[] red = new int[input.length];
        int[] blue = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            Color color = new Color(input[i]);
            green[i] = color.getGreen();
            red[i] = color.getRed();
            blue[i] = color.getBlue();
        }

        double rand;
        int result;
        int[] output = new int[input.length];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                rand = variance * (random.nextGaussian());
                result = (int) ((input[j * width + i] & 0xff) + rand);
                green[j * width + i] = (int) Math.abs(((green[j * width + i] & 0xFF) + rand)) % 256;
                red[j * width + i] = (int) Math.abs(((red[j * width + i] & 0xFF) + rand)) % 256;
                blue[j * width + i] = (int) Math.abs(((blue[j * width + i] & 0xFF) + rand)) % 256;
                tempImage.getRaster().getDataBuffer().setElem(j * width + i, new Color(red[j * width + i], green[j * width + i], blue[j * width + i]).getRGB());

            }
        }
        return tempImage;
    }

    public static BufferedImage greyGaussianNoise(BufferedImage img, double variance) {
        BufferedImage tempImage = Controller.deepCopy(img);
        int width = tempImage.getWidth();
        int height = tempImage.getHeight();
        Random random = new Random();
        int[] input = ((DataBufferInt) tempImage.getRaster().getDataBuffer()).getData();
        double rand;
        int result;
        int[] output = new int[input.length];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                rand = variance * (random.nextGaussian());
                result = (int) ((input[j * width + i] & 0xff) + rand);
                if (result < 0) {
                    result = 0;
                } else if (result > 255) {
                    result = 255;
                }
                output[j * width + i] = 0xff000000 | (result + (result << 16) + (result << 8));
            }
        }
        for (int i = 0; i < output.length; i++) {
            tempImage.getRaster().getDataBuffer().setElem(i, output[i]);
        }
        return tempImage;
    }

    public static BufferedImage greyCondimentNoise(BufferedImage img, double amount) {
        BufferedImage tempImage = Controller.deepCopy(img);
        int width = tempImage.getWidth();
        int height = tempImage.getHeight();
        Random random = new Random();
        int[] input = ((DataBufferInt) tempImage.getRaster().getDataBuffer()).getData();
        double rand;
        int result;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                rand = random.nextDouble();
                if (rand <= amount) {
                    rand = random.nextDouble();
                    if (rand < 0.5) {
                        result = 0;
                    } else {
                        result = 255;
                    }
                } else {
                    result = input[y * width + x] & 0xff;
                }
                tempImage.getRaster().getDataBuffer().setElem(y * width + x, 0xff000000 | (result + (result << 16) + (result << 8)));
            }
        }
        return tempImage;
    }

    @Deprecated
    public static BufferedImage colorCondimentNoise(BufferedImage img, double amount) {
        BufferedImage tempImage = Controller.deepCopy(img);
        int width = tempImage.getWidth();
        int height = tempImage.getHeight();
        Random random = new Random();
        int[] input = ((DataBufferInt) tempImage.getRaster().getDataBuffer()).getData();
        int[] green = new int[input.length];
        int[] red = new int[input.length];
        int[] blue = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            Color color = new Color(input[i]);
            green[i] = color.getGreen();
            red[i] = color.getRed();
            blue[i] = color.getBlue();
        }

        double rand;

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                rand = random.nextDouble();
                if (rand <= amount) {

                    rand = random.nextDouble();

                } else {
                    green[y * width + x] = Math.abs(green[y * width + x] & 0xff) % 256;
                    red[y * width + x] = Math.abs(red[y * width + x] & 0xff) % 256;
                    blue[y * width + x] = Math.abs(blue[y * width + x] & 0xff) % 256;
                }
                tempImage.getRaster().getDataBuffer().setElem(y * width + x, new Color(red[y * width + x], green[y * width + x], blue[y * width + x]).getRGB());
            }
        }
        return tempImage;
    }

    public static double[] createKernel(double sigma, int kernelsize) {
        double[] kernel = new double[kernelsize * kernelsize];
        for (int i = 0; i < kernelsize; i++) {
            double x = i - (kernelsize - 1) / 2;
            for (int j = 0; j < kernelsize; j++) {
                double y = j - (kernelsize - 1) / 2;
                kernel[j + i * kernelsize] = 1 / (2 * Math.PI * sigma * sigma) * Math.exp(-(x * x + y * y) / (2 * sigma * sigma));
            }
        }
        float sum = 0;
        for (int i = 0; i < kernelsize; i++) {
            for (int j = 0; j < kernelsize; j++) {
                sum += kernel[j + i * kernelsize];
            }
        }
        for (int i = 0; i < kernelsize; i++) {
            for (int j = 0; j < kernelsize; j++) {
                kernel[j + i * kernelsize] /= sum;
            }
        }
        return kernel;
    }


}

