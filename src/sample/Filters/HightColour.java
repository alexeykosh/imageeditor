package sample.Filters;

import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;


public class HightColour implements Filter {
    //0...255
    @Override
    public BufferedImage apply(BufferedImage bufferedImage, int bright, int contrast, int gray) {
        BufferedImage tempImage = Controller.deepCopy(bufferedImage);
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color(tempImage.getRGB(i, j));
                color = new Color(Math.min(color.getRed(), 255 - bright), Math.min(color.getGreen(), 255 - contrast), Math.min(color.getBlue(), 255 - gray), color.getAlpha());
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
        return tempImage;
    }
}
