package sample.Filters;

import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;

public class LowColour implements Filter {
    //0...255
    @Override
    public BufferedImage apply(BufferedImage bufferedImage, int bright, int contrast, int gray) {
        BufferedImage tempImage = Controller.deepCopy(bufferedImage);
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color((int) tempImage.getRGB(i, j));
                color = new Color(Math.max(color.getRed(), bright), Math.max(color.getGreen(), contrast), Math.max(color.getBlue(), gray), color.getAlpha());
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
        return tempImage;
    }
}
