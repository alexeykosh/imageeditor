package sample.Filters;

import java.awt.image.BufferedImage;


public interface SimpleFilter {
    BufferedImage apply(BufferedImage bufferedImage);
}
