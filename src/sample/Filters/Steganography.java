package sample.Filters;

import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;


public class Steganography implements SimpleFilter {

    public static BufferedImage hide(BufferedImage source, BufferedImage secret) {
        BufferedImage mySource = Controller.deepCopy(source);
        BufferedImage mySecret = Controller.deepCopy(secret);
        for (int i = 0; i < mySecret.getWidth() && i < mySource.getWidth(); i++) {
            for (int j = 0; j < mySecret.getHeight() && j < mySource.getHeight(); j++) {
                Color secretCol = new Color((int) mySecret.getRGB(i, j));
                Color finalCol = new Color((int) mySource.getRGB(i, j));
                finalCol = new Color(
                        (finalCol.getRed() & 252) ^ ((secretCol.getRed() & 192) >> 6),
                        (finalCol.getGreen() & 252) ^ ((secretCol.getGreen() & 192) >> 6),
                        (finalCol.getBlue() & 252) ^ ((secretCol.getBlue() & 192) >> 6),
                        finalCol.getAlpha()
                );
                mySource.setRGB(i, j, finalCol.getRGB());
            }
        }
        return mySource;
    }

    @Override
    public BufferedImage apply(BufferedImage bufferedImage) {
        BufferedImage tempImage = Controller.deepCopy(bufferedImage);
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color((int) tempImage.getRGB(i, j));
                color = new Color((color.getRed() & 3) << 6, (color.getGreen() & 3) << 6, (color.getBlue() & 3) << 6, color.getAlpha());
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
        return tempImage;
    }
}
