package sample.Filters;

import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;

public class WTFF implements SimpleFilter {
    @Override
    public BufferedImage apply(BufferedImage bufferedImage) {
        BufferedImage tempImage = Controller.deepCopy(bufferedImage);
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color((int) tempImage.getRGB(i, j));
                color = new Color(
                        Math.min(color.getRed(), color.getGreen()),
                        Math.min(color.getBlue(), color.getGreen()),
                        Math.min(color.getBlue(), color.getRed()),
                        color.getAlpha());
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
        return tempImage;
    }
}