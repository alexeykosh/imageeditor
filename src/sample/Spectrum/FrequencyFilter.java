package sample.Spectrum;

import org.jtransforms.fft.DoubleFFT_2D;
import sample.Controller.Controller;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

/**
 * Created by Alexey on 10.05.2017.
 */
public class FrequencyFilter {

    public static BufferedImage apply(BufferedImage img, String filterName) {
        BufferedImage tempImage = Controller.deepCopy(img);
        int width = 2 * Integer.highestOneBit(tempImage.getWidth());
        int height = 2 * Integer.highestOneBit(tempImage.getHeight());
        DoubleFFT_2D fft = new DoubleFFT_2D(width, height);
        double[][] input = new double[width][height];
        double[][] green = new double[width][height];
        double[][] red = new double[width][height];
        double[][] blue = new double[width][height];
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                input[i][j] = tempImage.getRGB(i, j);
                Color color = new Color((int) input[i][j]);

                green[i][j] = color.getGreen();
                green[i][j] *= Math.pow(-1, i + j);

                red[i][j] = color.getRed();
                red[i][j] *= Math.pow(-1, i + j);

                blue[i][j] = color.getBlue();
                blue[i][j] *= Math.pow(-1, i + j);
            }
        }

        fft.realForward(green);
        fft.realForward(red);
        fft.realForward(blue);


        double[][] H = new double[width][height];
        double radiusRed = getRadius(red, 0.8);
        double radiusGreen = getRadius(green, 0.5);
        double radiusBlue = getRadius(blue, 0.5);
        double d0 = Math.max(Math.max(radiusRed, radiusGreen), radiusBlue);
        switch (filterName) {
            case "lowBatt":
                for (int u = 0; u < width; u++) {
                    for (int v = 0; v < height; v++) {
                        double D = Math.sqrt((u - width / 2) * (u - width / 2) + (v - height / 2) * (v - height / 2));
                        H[u][v] = (double) 1 / (1 + Math.pow(D / d0, 4));

                    }
                }

                break;
            case "highGauss":
                for (int u = 0; u < width; u++) {
                    for (int v = 0; v < height; v++) {
                        double D = Math.sqrt((u - width / 2) * (u - width / 2) + (v - height / 2) * (v - height / 2));
                        H[u][v] = (double) 1 - Math.exp(-(D * D) / (2 * d0 * d0));
                    }
                }
                break;
            case "highBatt":
                for (int u = 0; u < width; u++) {
                    for (int v = 0; v < height; v++) {
                        double D = Math.sqrt((u - width / 2d) * (u - width / 2d) + (v - height / 2d) * (v - height / 2d));
                        H[u][v] = (double) 1 / (1 + Math.pow((d0 / D), 4));
                    }
                }
                break;
            case "lowGauss":
                for (int u = 0; u < width; u++) {
                    for (int v = 0; v < height; v++) {
                        double D = Math.sqrt((u - width / 2d) * (u - width / 2d) + (v - height / 2d) * (v - height / 2d));
                        H[u][v] = (Math.exp(-(D * D / (2 * d0 * d0))));
                    }
                }
                break;
        }


        for (int u = 0; u < width; u++) {
            for (int v = 0; v < height; v++) {
                red[u][v] *= H[u][v];
                green[u][v] *= H[u][v];
                blue[u][v] *= H[u][v];
            }
        }

        fft.realInverse(red, true);
        fft.realInverse(green, true);
        fft.realInverse(blue, true);

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                red[x][y] *= Math.pow(-1, x + y);
                green[x][y] *= Math.pow(-1, x + y);
                blue[x][y] *= Math.pow(-1, x + y);
            }
        }

//        for (int i = 0; i < width; i++) {
//            for (int j = 0; j < height; j++) {
//                if(red[i][j] > 255) red[i][j] = 255;
//                if(red[i][j] < 0) red[i][j] = 0;
//
//                if(green[i][j] > 255) green[i][j] = 255;
//                if(green[i][j] < 0) green[i][j] = 0;
//
//                if(blue[i][j] > 255) blue[i][j] = 255;
//                if(blue[i][j] < 0) blue[i][j] = 0;
//            }
//        }
        for (int i = 0; i < tempImage.getWidth(); i++) {
            for (int j = 0; j < tempImage.getHeight(); j++) {
                Color color = new Color(
                        Math.abs((int) red[i][j]) % 256,
                        Math.abs((int) green[i][j]) % 256,
                        Math.abs((int) blue[i][j]) % 256);
                tempImage.setRGB(i, j, color.getRGB());
            }
        }
//        for (int i = 0; i < width; i++) {
//            for (int j = 0; j < height; j++) {
//                Color color = new Color((int) red[i][j],
//                        (int) green[i][j],
//                        (int) blue[i][j]);
//                img.setRGB(i, j, color.getRGB());
//            }
//        }
        return tempImage;
    }

    public static BufferedImage spectrum(BufferedImage img) {
        BufferedImage tempImage = Controller.deepCopy(img);
        int[] pixels = ((DataBufferInt) tempImage.getRaster().getDataBuffer()).getData();
        FFT fft = new FFT(pixels, tempImage.getWidth(), tempImage.getHeight());
        int[] result = fft.getPixels();
        for (int i = 0; i < result.length; i++) {
            tempImage.getRaster().getDataBuffer().setElem(i, result[i]);
        }
        return tempImage;
    }


    public static int getRadius(double[][] array, double alpha) {
        double PT = 0, tempAlpha = 0;
        int radius, y0 = array[0].length / 2, x0 = array.length / 2;
        for (int u = 0; u < array.length; u++) {
            for (int v = 0; v < array[0].length; v++) {
                PT += array[u][v] * array[u][v];
            }
        }
        label:
        for (radius = 0; radius < Math.min(x0, y0); radius++) {
            for (int v = y0 - radius; v < y0 + radius; v++) {
                tempAlpha += (array[x0 - radius][v] * array[x0 - radius][v] / PT);
                if (tempAlpha >= alpha) break label;
            }
            for (int v = y0 - radius; v < y0 + radius; v++) {
                tempAlpha += (array[x0 + radius][v] * array[x0 + radius][v] / PT);
            }
            for (int u = x0 - radius; u < x0 + radius; u++) {
                tempAlpha += (array[u][y0 - radius] * array[u][y0 - radius] / PT);
                if (tempAlpha >= alpha) break label;
            }
            for (int u = x0 - radius; u < x0 + radius; u++) {
                tempAlpha += (array[u][y0 + radius] * array[u][y0 + radius] / PT);
                if (tempAlpha >= alpha) break label;
            }
        }
        return radius;
    }
}
